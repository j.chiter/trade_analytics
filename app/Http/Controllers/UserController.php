<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Services\UserService;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function count(): int
    {
        return $this->userService->getAllCount();
    }

    public function store(UserRequest $userRequest): array
    {
        return $this->userService->storeOrUpdate($userRequest);
    }
}
