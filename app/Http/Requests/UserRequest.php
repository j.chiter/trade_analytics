<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'users' => 'required|array',
            'users.*.email' => ['required'],
            'users.*.first_name' => 'required|max:100',
            'users.*.last_name' => 'required|max:100',
            'users.*.age' => 'required|integer'
        ];
    }
}
