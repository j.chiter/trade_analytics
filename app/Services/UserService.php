<?php

namespace App\Services;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Database\Query\Expression;

class UserService
{
    private static function getFullName($user): string
    {
        return $user['first_name'] . ' ' . $user['last_name'];
    }

    public function getAllCount(): int
    {
        return User::count();
    }

    public function storeOrUpdate(UserRequest $userRequest): array
    {
        $toUpdatedUsers = [];
        $fullNames = [];
        $users = collect($userRequest->get('users'));
        $users->map(function ($item) use (&$fullNames) {
            $name = self::getFullName($item);
            if (!in_array($name, $fullNames)) {
                $fullNames[$name] = $item;
            }
        });
        User::whereIn(new Expression('CONCAT_WS(\' \', first_name, last_name)'), array_keys($fullNames))
            ->get()
            ->map(function ($user) use (&$toUpdatedUsers) {
                $toUpdatedUsers[self::getFullName($user)] = $user;
            });

        $storeUsers = [];
        $updatedUsers = [];
        foreach ($users as $user) {
            $name = self::getFullName($user);
            if (!array_key_exists($name, $toUpdatedUsers)) {
                $storeUsers[] = $user;
            }
        }

        $insertedCount = User::insertOrIgnore($storeUsers);
        $updatedCount = 0;
        foreach ($toUpdatedUsers as $updatedUserName => $updatedUser) {
            $updatedUser
                ->fill([
                    'age' => $fullNames[$updatedUserName]['age'],
                    'email' => $fullNames[$updatedUserName]['email']
                ])
                ->save();
            $updatedCount++;
        }

        return [
            'count' => $this->getAllCount(),
            'inserted' => $insertedCount,
            'updated' => $updatedCount
        ];
    }
}
