<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('users')->group(function () {
    Route::get('count', [UserController::class, 'count']);
    Route::post('store', [UserController::class, 'store']);
});
