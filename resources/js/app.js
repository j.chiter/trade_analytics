import './bootstrap';
import { createApp } from 'vue';
import Application from './layout/Application.vue';
import '../sass/app.scss';
import api from "./api/api.js";

const app = createApp(Application);
app.config.globalProperties.$api = api;

app.mount('#app');
